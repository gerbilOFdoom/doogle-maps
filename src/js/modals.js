import MicroModal from 'micromodal';

export function init() {
  console.log('[MODALS] init()');

  MicroModal.init();
}