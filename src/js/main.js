// Insted of Polyfill use packages directly
import 'core-js/stable';
import 'regenerator-runtime/runtime';

import * as preferences from './preferences.js';
import * as modals from './modals.js';
import * as tabs from './tabs.js';
import * as toggles from './toggles.js';
import * as search from './search.js';
import * as map from './map.js';

console.log('[MAIN] setting up...');

preferences.init();
map.bootstrap();
search.init();
modals.init();
tabs.init();
toggles.init();
