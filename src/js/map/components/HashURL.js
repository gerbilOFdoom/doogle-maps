/*
 * Code based on: https://github.com/mlevans/leaflet-hash
 * Original under MIT License
 * Copyright (c) 2013 Michael Lawrence Evans
 */
import L from 'leaflet';

(function(window) {
  var HAS_HASHCHANGE = (function() {
    var doc_mode = window.documentMode;
    return ('onhashchange' in window) &&
      (doc_mode === undefined || doc_mode > 7);
  })();

  L.Hash = function(map) {
    this.onHashChange = L.Util.bind(this.onHashChange, this);

    if (map) {
      this.init(map);
    }
  };

  L.Hash.prototype = {
    map: null,
    lastHash: null,

    /*
     * RS hash: #{zoom}/{mapID}/{plane}/{x}/{y}
     */
    parseHash: function(hash) {
      if(hash.indexOf('#') === 0) {
        hash = hash.substr(1);
      }
      var args = hash.split('/');
      if (args.length === 5) {
        var zoom, mapID, plane, x, y;
        zoom = parseInt(args[0]);
        mapID = parseInt(args[1]);
        plane = parseInt(args[2]);
        x = parseInt(args[3]);
        y = parseInt(args[4]);
        if (isNaN(zoom) || isNaN(mapID) || isNaN(plane) ||
            isNaN(x) || isNaN(y)) {
          return false;
        } else {
          return {
            mapID: mapID,
            plane: plane,
            center: [y, x],
            zoom: zoom,
          };
        }
      } else {
        return false;
      }
    },

    formatHash: function(map) {
      var center, zoom, mapID, plane, precision;
      center = map.getCenter();
      zoom = map.getZoom();
      mapID = map.getMapID();
      plane = map.getPlane();
      precision = Math.max(0, Math.ceil(Math.log(zoom) / Math.LN2));

      return '#' + [zoom,
        mapID,
        plane,
        Math.floor(center.lng.toFixed(precision)),
        Math.floor(center.lat.toFixed(precision)),
      ].join('/');
    },

    init: function(map) {
      this.map = map;

      // reset the hash
      this.lastHash = null;
      this.onHashChange();

      if (!this.isListening) {
        this.startListening();
      }
    },

    removeFrom: function(map) {
      if (this.changeTimeout) {
        clearTimeout(this.changeTimeout);
      }

      if (this.isListening) {
        this.stopListening();
      }

      this.map = null;
    },

    onMapMove: function() {
      // bail if we're moving the map (updating from a hash),
      // or if the map is not yet loaded

      if (this.movingMap || !this.map._loaded) {
        return false;
      }

      var hash = this.formatHash(this.map);
      if (this.lastHash !== hash) {
        location.replace(hash);
        this.lastHash = hash;
      }
    },

    movingMap: false,
    update: function() {
      var hash = location.hash;
      if (hash === this.lastHash) {
        return;
      }
      var parsed = this.parseHash(hash);
      if (parsed) {
        this.movingMap = true;

        this.map.initView( parsed.mapID, parsed.plane, parsed.center,
          parsed.zoom );

        this.movingMap = false;
      } else {
        this.map.initDefaultView();
        this.onMapMove(this.map);
      }
    },

    // defer hash change updates every 100ms
    changeDefer: 100,
    changeTimeout: null,
    onHashChange: function() {
      // throttle calls to update() so that they only happen every
      // `changeDefer` ms
      if (!this.changeTimeout) {
        var that = this;
        this.changeTimeout = setTimeout(function() {
          that.update();
          that.changeTimeout = null;
        }, this.changeDefer);
      }
    },

    isListening: false,
    hashChangeInterval: null,
    startListening: function() {
      this.map.on('moveend', this.onMapMove, this);
      this.map.on('mapidchanged', this.onMapMove, this);
      this.map.on('planechanged', this.onMapMove, this);

      if (HAS_HASHCHANGE) {
        L.DomEvent.addListener(window, 'hashchange', this.onHashChange);
      } else {
        clearInterval(this.hashChangeInterval);
        this.hashChangeInterval = setInterval(this.onHashChange, 50);
      }
      this.isListening = true;
    },

    stopListening: function() {
      this.map.off('moveend', this.onMapMove, this);
      this.map.off('mapidchanged', this.onMapMove, this);
      this.map.off('planechanged', this.onMapMove, this);

      if (HAS_HASHCHANGE) {
        L.DomEvent.removeListener(window, 'hashchange', this.onHashChange);
      } else {
        clearInterval(this.hashChangeInterval);
      }
      this.isListening = false;
    },
  };
  L.hash = function(map) {
    return new L.Hash(map);
  };
  L.Map.prototype.addHash = function() {
    this._hash = L.hash(this);
  };
  L.Map.prototype.removeHash = function() {
    this._hash.removeFrom();
  };
})(window);
