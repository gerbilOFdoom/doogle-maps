//import { _map, config, layerDataLoader }  from '../map';
import * as utils from '../utils';
import * as preferences from '../preferences';
import * as search from '../search';
import L from 'leaflet';

// Coords
export const COORDS_LUMBRIDGE = [ 3225, 3219 ];
export const COORDS_LUMBRIDGE_2 = [ 3228, 3233 ];

export let _layer;

var _fromPoint;
var _toPoint;
var _mapCreator;

export function init(mapCreator) {
  _mapCreator = mapCreator;
  console.log('[MAP/PATHFIND] init()');

  _fromPoint = L.point(COORDS_LUMBRIDGE);
  _toPoint = L.point(COORDS_LUMBRIDGE_2);

  mapCreator._map.on('click', onMapClick);
  mapCreator._map.on('contextmenu', onMapRightClick);

  _layer = L.layerGroup().addTo(mapCreator._map);
}



function onMapClick(e) {
  _fromPoint = L.point(Math.floor(e.latlng.lng), Math.floor(e.latlng.lat));

  console.log('Clicked:', _fromPoint);

  clear();
  drawPath(_fromPoint, _toPoint);
}

function onMapRightClick(e) {
  let _toPoint = L.point(Math.floor(e.latlng.lng), Math.floor(e.latlng.lat));

  console.log('Clicked (R):', _toPoint);

  clear();
  drawPath(_fromPoint, _toPoint);
}

export function clear() {
  _layer.clearLayers();
}

export async function drawPath(from, to, options = {}) {
  // Set default options
  if(options.startMarker === undefined){
    options.startMarker = true;
  }
  if(options.endMarker === undefined){
    options.endMarker = true;
  }
  if(options.showDirectionDetails === undefined){
    options.showDirectionDetails = true;
  }

  console.log('[PATHFINDER] from:', from, 'to:', to);
  let url = _mapCreator.config.basePathURL + 'pathfinder/path.json?from=' + from.x + ',' + from.y + '&to=' + to.x + ',' + to.y;

  // let bypassCors = true;
  // if(bypassCors){
  // 	url = crossDomain(url);
  // }
  var iconStore = await _mapCreator.layerDataLoader.getIconStore();

  if(options.startMarker){
    var greenPin = iconStore.getIcon(0, 'greenPin');
    drawMarker(from.x, from.y, {icon: greenPin}, 'from');
  }
  if(options.endMarker){
    var redPin = iconStore.getIcon(0, 'redPin');
    drawMarker(to.x, to.y, {icon: redPin}, 'to');
  }

  try{
    var responce = await fetch(url);
    var data = await responce.json();
    if(!data) {
      console.warn('[MAP/PATHFIND]', 'Unable to fetch path for specified coords. Try again.');
      return;
    }

    search.showPath(undefined, data, options);
  }catch(e){
    console.error('Error: Pathfinder: '+ e.message);
  }
}

// Usage: drawMarker(...[x, y])
export function drawMarker(x, y, opts = {}, name = '') {
  // console.log("Creating marker at:",x,y);
  let marker = L.marker([ y + 0.5, x + 0.5 ], opts);
  marker.bindPopup(`Location: (${x},${y})<br/>Name: ${name}`).openPopup();
  _layer.addLayer(marker);
  return marker;
}

export function drawLine(points, color = 'blue', dash = '0.6,5') {
  // Check if it's walking or a teleport
  if(points.length === 2) {
    let distX = Math.abs(points[1].x - points[0].x);
    let distY = Math.abs(points[1].y - points[0].y);
    if(distX > 1 || distY > 1) {
      // It's a teleport
      color = 'magenta';
      dash = '1';
    }
  }

  let pointsFix = [];
  for(let coord of points) {
    pointsFix.push([ coord.y + 0.5, coord.x + 0.5 ]);
  }

  let line = L.polyline(pointsFix, { 'color': color, 'dashArray': dash });
  _layer.addLayer(line);
}

function crossDomain(url, via = undefined) {
  switch (via) {
    case 'crossorigin':
      return 'http://crossorigin.me/' + url;
    case 'whateverorigin':
      return 'http://whateverorigin.org/get?url=' + encodeURIComponent(url) + '&callback=?';
    case 'anyorigin':
      return 'http://anyorigin.com/go/?url=' + encodeURIComponent(url) + '&callback=?';
    case 'corsanywhere':
    default:
      return 'https://cors-anywhere.herokuapp.com/' + url;
  }
}
